var EMAIL_BOX_PREFIX = 'email_box_';

function set_email(data, params) {
	var email = data.text;
	var member_id = params.member_id;
	var email_box_id = EMAIL_BOX_PREFIX + member_id;
	
	$('#' + email_box_id).find('a').hide();
	$('#' + email_box_id).find('span').text(email);
	$('#' + email_box_id).find('span').show();
}

function get_email(member_id) {
	$.ceAjax(	'request',
				fn_url('stuff.get_email?id=' + member_id), {	
					method: 'post', 
					callback: set_email,
					member_id: member_id});
}

$(document).ready(function() {
	var members = $('.member-email-box');
	members.click(function() {
		var member_id = $(this).attr('id').replace(EMAIL_BOX_PREFIX, '');
		get_email(member_id);
		return false;
	});
	
	$(document).ready(function() {
		$(".team-slide ul").owlCarousel({            
            itemsDesktop:[2000,7],
            itemsDesktopSmall:[1300,6],
            itemsTablet:[1150,4],
            itemsMobile:[767,1],
            navigation : true,
			pagination:false,
            navigationText : ['<i class="bx-prev icon-car-left"></i>','<i class="bx-next icon-car-right"></i>'],
			rewindNav : true,
            slideSpeed : slideSpeed,
            paginationSpeed : 800,
            rewindSpeed : 500,
			scrollPerPage : false});
	});
});