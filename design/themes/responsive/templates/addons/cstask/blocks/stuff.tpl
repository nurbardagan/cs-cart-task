{assign var="obj_prefix" value="`$block.block_id`000"}

<script>
	var slideSpeed = {$block.properties.speed};
</script>

{if $block.properties.set_order == 'Y'}
	{$members|@sort_members}
{/if}

<div class="team-slide">
    <ul>
		{foreach from=$members item="member" name="for_brands"}
			{assign var="obj_id" value="scr_`$block.block_id`000`$member.id`"}
			{include 	file="common/image.tpl" 
						assign="object_img" 
						images=$member.image_pair 
						no_ids=true}
			<li class="item">
				{$object_img nofilter}
				<h4>{$member.first_name} {$member.last_name}</h4>
				{$member.member_function}
				<div id="email_box_{$member.id}" class="member-email-box">
					<a href="#">Показать email</a>
					<span class="member-email"></span>
				</div>
			</li>
		{/foreach}
	</ul>
</div>