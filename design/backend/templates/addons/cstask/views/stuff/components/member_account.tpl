{include file="common/subheader.tpl" title=__("member_account_information")}

<div class="control-group">
    <label for="first_name" class="control-label">{__("first_name")}:</label>
    <div class="controls">
        <input type="text" id="first_name" name="member_data[first_name]" class="input-large" size="32" maxlength="128" value="{$member_data.first_name}" />
    </div>
</div>

<div class="control-group">
    <label for="last_name" class="control-label">{__("last_name")}:</label>
    <div class="controls">
        <input type="text" id="last_name" name="member_data[last_name]" class="input-large" size="32" maxlength="128" value="{$member_data.last_name}" />
    </div>
</div>

<div class="control-group">
    <label for="email" class="control-label cm-required cm-email">{__("email")}:</label>
    <div class="controls">
        <input type="text" id="email" name="member_data[email]" class="input-large" size="32" maxlength="128" value="{$member_data.email}" />
    </div>
</div>

<div class="control-group">
	<label for="location_meta_descr" class="control-label">{__("member_function")}: </label>
	<div class="controls">
		<textarea id="function" name="member_data[member_function]" cols="54" rows="8" class="cm-wysiwyg span9">{$member_data.member_function}</textarea>
	</div>
</div>

<div class="control-group">
	<label class="control-label">{__("icon")}</label>
	<div class="controls">
		{include file="common/attach_images.tpl" image_name="member_image" image_object_type="member_image" hide_titles=true no_detailed=true image_pair=$member_data.image_pair}
	</div>
</div>

<div class="control-group">
    <label for="user_link" class="control-label">{__("user_link")} (eMail):</label>
    <div class="controls">
        <input type="text" id="user_link" name="member_data[user_link]" class="input-large" size="32" maxlength="128" value="{$member_data.user_link}" />
    </div>
</div>