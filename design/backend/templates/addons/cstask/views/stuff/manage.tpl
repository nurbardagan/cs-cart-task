{include file="views/profiles/components/profiles_scripts.tpl"}

{capture name="mainbox"}

<form action="{""|fn_url}" method="post" name="members_form" id="members_form">

{include file="common/pagination.tpl" save_current_page=true save_current_url=true div_id=$smarty.request.content_id}

{assign var="c_url" value=$config.current_url|fn_query_remove:"sort_by":"sort_order"}

{assign var="rev" value=$smarty.request.content_id|default:"pagination_contents"}

{if $members}
<table width="100%" class="table table-middle">
<thead>
<tr>
    <!-- TODO: implement -->
	<!--<th width="1%" class="center {$no_hide_input}">
        {include file="common/check_items.tpl"}</th>-->
    <th width="4%" class="nowrap"><a class="cm-ajax" href="{"`$c_url`&sort_by=id&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("id")}{if $search.sort_by == "id"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
    <th width="18%"><a class="cm-ajax" href="{"`$c_url`&sort_by=name&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("member_name")}{if $search.sort_by == "name"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
    <th width="18%"><a class="cm-ajax" href="{"`$c_url`&sort_by=function&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("member_function")}{if $search.sort_by == "function"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
    <th width="20%"><a class="cm-ajax" href="{"`$c_url`&sort_by=email&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("email")}{if $search.sort_by == "email"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
    <th width="20%"><a class="cm-ajax" href="{"`$c_url`&sort_by=user&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("user_link")}{if $search.sort_by == "user"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
</tr>
</thead>
{foreach from=$members item=member}

<tr>
    <!--<td class="center {$no_hide_input}">
        <input type="checkbox" name="member_ids[]" value="{$member.id}" class="checkbox cm-item" /></td>-->
    <td><a class="row-status" href="{"stuff.update?member_id=`$member.id`"|fn_url}">{$member.id}</a></td>
    <td class="row-status"><a href="{"stuff.update?member_id=`$member.id`"|fn_url}">{if $member.first_name || $member.last_name}{$member.last_name} {$member.first_name}{else}-{/if}</a></td>
    <td class="row-status"><a href="{"stuff.update?member_id=`$member.id`"|fn_url}">{$member.member_function}</a></td>
    <td><a class="row-status" href="mailto:{$member.email|escape:url}">{$member.email}</a></td>
	<td class="row-status"><a href="{"stuff.update?member_id=`$member.id`"|fn_url}">{$member.user_link}</a></td>
</tr>
{/foreach}
</table>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}

{include file="common/pagination.tpl" div_id=$smarty.request.content_id}

{capture name="buttons"}
    {if $users}
        {capture name="tools_list"}
            {if "ULTIMATE"|fn_allowed_for || !$runtime.company_id}
                {hook name="profiles:list_tools"}
                    <li>{btn type="list" text=__("export_selected") dispatch="dispatch[profiles.export_range]" form="userlist_form"}</li>
                {/hook}
            {/if}
            <li>{btn type="delete_selected" dispatch="dispatch[profiles.m_delete]" form="userlist_form"}</li>
        {/capture}
        {dropdown content=$smarty.capture.tools_list}
    {/if}
{/capture}
</form>
{/capture}

{capture name="adv_buttons"}
	{assign var="_title" value=__("members")}

	<a class="btn cm-tooltip" href="{"stuff.add"|fn_url}" title="{__("add_member")}"><i class="icon-plus"></i></a>
{/capture}

{include file="common/mainbox.tpl" title=$_title content=$smarty.capture.mainbox adv_buttons=$smarty.capture.adv_buttons buttons=$smarty.capture.buttons content_id="manage_users"}