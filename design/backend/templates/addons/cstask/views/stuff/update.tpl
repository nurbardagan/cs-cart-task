{if $member_data.id}
    {assign var="id" value=$member_data.id}
{else}
    {assign var="id" value=0}
{/if}

<form name="member_form" action="{""|fn_url}" method="post" class="form-horizontal form-edit form-table" enctype="multipart/form-data">
{capture name="mainbox"}
	{capture name="tabsbox"}
		<input type="hidden" name="member_id" value="{$id}" />
		<input type="hidden" class="cm-no-hide-input" name="selected_section" id="selected_section" value="{$selected_section}" />
		
		<div id="content_general">
			{include file="addons/cstask/views/stuff/components/member_account.tpl"}

			{if $settings.General.user_multiple_profiles == "Y" && $id}
				{include file="common/subheader.tpl" title=__("user_profile_info")}
				<p class="form-note">{__("text_multiprofile_notice")}</p>
				{include file="views/profiles/components/multiple_profiles.tpl"}
			{/if}

		</div>
		
	{/capture}
	
	{include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox group_name=$runtime.controller active_tab=$selected_section track=true}
{/capture}

{if !$id}
    {assign var="_title" value=__("new_member")}
{else}
    {if $member_data.firstname}
        {assign var="_title" value="{__("editing_member")}: `$member_data.firstname` `$member_data.lastname`"}
	{else}
        {assign var="_title" value="{__("editing_member")}: `$member_data.email`"}
    {/if}
{/if}

{$_title = $_title|strip_tags}
{assign var="redirect_url" value="stuff.manage"}

{capture name="buttons"}
	<div class="btn-group btn-hover dropleft">
		{include file="buttons/button.tpl" but_text=__("save") but_meta="dropdown-toggle" but_role="submit-link" but_name="dispatch[stuff.update]" but_target_form="member_form"}
	</div>
	<div class="btn-group btn-hover dropleft">
		{include file="buttons/button.tpl" but_text=__("delete") but_meta="dropdown-toggle" but_role="submit-link" but_name="dispatch[stuff.delete]" but_target_form="member_form"}
	</div>
{/capture}

{include file="common/mainbox.tpl" title=$_title content=$smarty.capture.mainbox buttons=$smarty.capture.buttons}
</form>
