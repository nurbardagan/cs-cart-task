<?php

$schema['addons/cstask/blocks/stuff.tpl'] = array (
	'settings' => array(
		'speed' =>  array (
			'type' => 'input',
			'default_value' => 400
		),
		'set_order' => array (
			'type' => 'checkbox',
			'default_value' => 'Y'
		)
	),
);

return $schema;