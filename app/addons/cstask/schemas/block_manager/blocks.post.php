<?php

$schema['stuff'] = array(
	'content' => array(
        'members' => array(
            'type' => 'function',
            'function' => array('fn_get_members')
        )
    ),
    'templates' => array('addons/cstask/blocks/stuff.tpl' => array()),
    'wrappers' => 'blocks/wrappers',
	'cache' => array(
		'disable_cache_when' => array(
			'update_handlers' => array('stuff')
		)
	)
);

return $schema;