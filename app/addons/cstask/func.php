<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_get_members() {
	$members = db_get_array("SELECT * FROM ?:stuff");
	
	foreach ($members as &$member) {
		$member['image_pair'] = fn_get_image_pairs($member['id'], 'member_image', 'M');
		link_user($member);
	}
	
	return $members;
}

function link_user(&$row) {
	if ($row['user_link']) {
		$user = db_get_row("SELECT * FROM ?:users WHERE email = ?s", $row['user_link']);
		
		if ($user) {
			$row['first_name'] = $user['firstname'];
			$row['last_name'] = $user['lastname'];
			$row['email'] = $user['email'];
		}
	}
}

function sort_members(&$members) {
	foreach ($members as $key => $member) {
		$names[$key] = $member['first_name'] . $member['last_name'];
	}
	
	error_log(print_r($names, true));
	array_multisort($names, SORT_ASC, $members);
	error_log(print_r($names, true));
}