<?php

use Tygh\Tools\SecurityHelper;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

fn_define('KEEP_UPLOADED_FILES', true);

if ($_SERVER['REQUEST_METHOD']	== 'POST') {

    if ($mode == 'update' && isset($_REQUEST['member_data'])) {
		$member_data = $_REQUEST['member_data'];
		
		fn_trusted_vars('member_data');		
		SecurityHelper::sanitizeObjectData('member_data', $member_data);
		
		if (!empty($_REQUEST['member_id'])) {
			$member_id = $_REQUEST['member_id'];
		}
		
		if (empty($member_id)) {
			$member_data['member_id'] = $member_id = db_query('INSERT INTO ?:stuff ?e', $member_data);
		} else {
			db_query("UPDATE ?:stuff SET ?u WHERE id = ?i", $member_data, $member_id);
		}
		
		fn_attach_image_pairs('member_image', 'member_image', $member_id);
    } elseif ($mode == 'delete') {
		if (isset($_REQUEST['member_id'])) {
            $id = $_REQUEST['member_id'];
			db_query("DELETE FROM ?:stuff WHERE id = ?i", $id);
        }
    }

    return array(CONTROLLER_STATUS_REDIRECT, 'stuff.manage');
}

if ($mode == 'manage') {
	$data = db_get_array("SELECT * FROM ?:stuff");
	
	foreach ($data as &$row)
		link_user($row);
	
	Tygh::$app['view']->assign('members', $data);
} elseif ($mode == 'update') {
	$member_id = $_REQUEST['member_id'];
	
	$data = db_get_row("SELECT * FROM ?:stuff WHERE id = ?i", $member_id);
	
	link_user($data);
	
    if (!empty($data)) {
		$data['image_pair'] = fn_get_image_pairs($member_id, 'member_image', 'M');
    }
	
	//fn_print_die($data);
	Tygh::$app['view']->assign('member_data', $data);	
}
