<?php

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD']	== 'POST') {
	if ($mode == 'get_email') {
		$id = $_REQUEST['id'];
		$email = "Not found";
		
		if (isset($id)) {
			$data = db_get_row("SELECT * FROM ?:stuff WHERE id = ?i", $id);
			link_user($data);

			if ($data) $email = $data['email'];
		}
		
		Tygh::$app['view']->assign('email', $email);
	}
}